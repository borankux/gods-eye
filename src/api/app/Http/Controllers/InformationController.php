<?php

namespace App\Http\Controllers;

use Linfo\Exceptions\FatalException;
use Linfo\Linfo;
use Rych\ByteSize\ByteSize;
class InformationController extends Controller
{

    public function getSystemInfo()
    {
        try {
            $linfo = new Linfo();
            $parser = $linfo->getParser();
            $battery = $parser->getBattery();
            $ram  = $parser->getRam();

            $ps = $parser->getProcessStats();
            $info = [
                'battery' => $battery,
                'total' => ByteSize::formatMetric($ram["total"]),
                'free' => ByteSize::formatMetric($ram["free"]),
                'ps' => $ps,
                'uptime' => $parser->getUpTime()
            ];
            dd($info);
        } catch (FatalException $e) {
            echo "doesn't support";
        }
    }
}
