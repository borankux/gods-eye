<?php

class SwooleTest{

    public function run () {
        $server = new swoole_websocket_server("0.0.0.0", 8081);

        $server->on('open', function($server, $req) {
            echo "connection open: {$req->fd}\n";
        });


        $server->on('message', function($server, $frame) {
//            $linfo = new \Linfo\Linfo();
//            $parser = $linfo->getParser();
//            $data = $parser->getHD();
            $server->push($frame->fd, json_encode($data));
        });

        $server->on('close', function($server, $fd) {
            echo "connection close: {$fd}\n";
        });


        $server->start();
    }
}