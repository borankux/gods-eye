let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);

const si = require('systeminformation');

let port = 8033;
server.listen(port);
// WARNING: app.listen(80) will NOT work here!

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

async  function getInformation(){
    const data = [];
    const cpu  = await si.cpu();
    const mem  = await si.mem();
    const disk = await si.battery();

    data.push(cpu);
    data.push(mem);
    data.push(disk);

    console.log(data);
    return data;
}


io.on('connection', function (socket) {

    socket.emit('init', {
        init: 'connection is fine !'
    });

    socket.on('info', function(data) {
        socket.emit('info', {
            origin: data,
            info : 'server is responding: timestamp:' + Date.now(),
        });

        si.cpu(function(data){
            socket.emit('info', data);
        });
    })
});

console.log("listening on "+ port);